const path = require('path');
const fs = require('fs');

// check if file exists: fs.existsSync('/path/to/file')

const obj = {
	index: path.join(path.resolve(__dirname, 'src/js'), 'index.js'),
}

module.exports = obj